<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function create()
    {
        return view('contacts.index');
    }

    public function store(Request $request)
    {
        $requestData = $request->validate([
            'email' => ['required'],
            'message' => ['required'],
        ]);

        $contact = new Contact();
        $contact->email = $requestData['email'];
        $contact->message = $requestData['message'];
        $contact->save();

        return redirect()->route('main');
    }
}
