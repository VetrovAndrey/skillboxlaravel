<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use App\Models\Tag;
use App\Models\Task;
use App\Services\TagsSynchronizer;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = Article::with('user')->latest()->get();
        return view('index', ['articles' => $articles]);
    }

    public function show(Article $article)
    {
        return view('articles.detail', ['article' => $article]);
    }

    public function store(ArticleRequest $request, TagsSynchronizer $tagSync)
    {
        $requestData = $request->validated();

        $article = new Article();
        $article->slug = $requestData['slug'];
        $article->name = $requestData['name'];
        $article->previewText = $requestData['previewText'];
        $article->detailText = $requestData['detailText'];
        $article->published = (bool) request('published');
        $article->user_id = request()->user()->id;
        $article->save();

        $tags = collect(explode(', ', $request->get('tags')));

        if ($tags->isNotEmpty()) {
            $tagSync->sync($article, $tags);
        }

        return redirect()->route('main');
    }

    public function create()
    {
        //Форма создания статьи
        return view('articles.create', ['article' => new Article()]);
    }

    public function destroy(Article $article)
    {
        $article->delete();
        return redirect()->route('main');
    }

    public function edit(Article $article)
    {
        return view('articles.edit', ['article' => $article]);
    }

    public function update(Article $article, ArticleRequest $request, TagsSynchronizer $tagSync)
    {
        $requestData = $request->validated();

        $article->update(array_merge($requestData, ['published' => (bool) $request->get('published')]));

        $tags = $article->tags->keyBy('name');
        $requestTags = collect(explode(', ', $request->get('tags')))->keyBy(function ($item) {return $item;});

        $tagsToAttach = $requestTags->diffKeys($tags);
        $tagsToDetach = $tags->diffKeys($requestTags);

        $tagSync->sync($article, $tagsToAttach, $tagsToDetach);

        return redirect()->route('main');
    }

    public function showByTag(Tag $tag)
    {
        return view('index', ['articles' => $tag->articles()->latest()->get()]);
    }
}
