<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ArticleRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'slug' => ['required', 'alpha_dash', Rule::unique('articles', 'slug')->ignore($this->article)],
            'name' => ['required', 'string', 'min:5', 'max:100'],
            'previewText' => ['required', 'string', 'max:255'],
            'detailText' => ['required', 'string'],
        ];
    }
}
