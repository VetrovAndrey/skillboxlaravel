<?php

namespace App\Providers;

use App\Models\Tag;
use App\Services\TagsSynchronizer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TagsSynchronizer::class, TagsSynchronizer::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('tags.cloud', function ($view) {
            $tags = collect();

            foreach (Tag::all() as $tag) {
                if ($tag->articles->isNotEmpty()) {
                    $tags->push($tag);
                }
            }

            $view->with('tags', $tags);
        });

        $this->bladeDirectives();
    }

    public function bladeDirectives()
    {
        Blade::if('articleOwner', function ($article) {
            if (Auth::check()) {
                return Auth::user()->id == $article->user->id;
            }

            return false;
        });
    }
}
