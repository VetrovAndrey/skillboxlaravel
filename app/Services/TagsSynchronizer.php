<?php

namespace App\Services;

use App\Models\Tag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class TagsSynchronizer
{
    public function sync(Model $model, Collection $tagsAttach, Collection $tagsDetach = null)
    {
        foreach ($tagsAttach as $tag) {
            $tag = Tag::firstOrCreate(['name' => trim($tag)]);
            $model->tags()->attach($tag);
        }

        if ($tagsDetach) {
            foreach ($tagsDetach as $tag) {
                $model->tags()->detach($tag);
            }
        }
    }
}
