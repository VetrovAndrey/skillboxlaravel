@extends('layouts.master')

@section('title', 'Главная страница')

@section('content')
    <div class="col-md-8 blog-main">
        <h3 class="pb-3 mb-4 font-italic border-bottom">
            Список задач
        </h3>

        @if($articles->isNotEmpty())
            @foreach($articles as $article)
                <div class="blog-post">
                    <a href="{{ route("articles.detail", ["article" => $article]) }}">
                        <h2 class="blog-post-title">{{ $article->name }}</h2>
                    </a>

                    @if ($article->tags->isNotEmpty())
                        <div>
                            @foreach($article->tags as $tag)
                                <span class="badge bg-primary">{{ $tag->name }}</span>
                            @endforeach
                        </div>
                    @endif

                    @if($article->user)
                        <p class="blog-post-meta">Владелец: {{ $article->user->name }}</p>
                    @endif

                    <p class="blog-post-meta">{{ $article->created_at->format('F j, Y') }}</p>
                    <p>{{ $article->previewText }}</p>
                </div>

                <hr>
            @endforeach
        @else
            <div class="bg-danger">
                <h2>Статей нет</h2>
            </div>
        @endif
    </div>
@endsection
