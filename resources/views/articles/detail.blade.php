@extends('layouts.master')

@section('title', $article->name)

@section('content')
    <div class="col-md-8">
        <div class="blog-post">
            <h2 class="blog-post-title">{{ $article->name }}</h2>
            <p class="blog-post-meta">{{ $article->created_at->format('F j, Y') }}</p>
            <p>{{ $article->detailText }}</p>
        </div>

        @articleOwner($article)
            <div style="display: flex">
                <a href="{{ route('articles.edit', ['article' => $article]) }}" class="btn btn-primary" style="margin-right: 10px"> Изменить </a>

                <form action="{{ route('articles.destroy', ['article' => $article]) }}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger">Удалить</button>
                </form>
            </div>
        @endarticleOwner

        <a href="/" class="btn btn-link">К списку статей</a>
    </div>
@endsection
