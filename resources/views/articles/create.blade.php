@extends('layouts.master')

@section('title', 'Создание статьи')

@section('content')
    <h3 class="pb-3 mb-4 font-italic border-bottom">
        Создание статьи
    </h3>

    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach
    @endif

    <form class="col-md-8" action="{{ route("articles.store") }}" method="post">
        @csrf
        @include('articles.formFields')
        <button type="submit" class="btn btn-primary">Создать</button>
    </form>
@endsection
