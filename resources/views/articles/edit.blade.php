@extends('layouts.master')

@section('title', 'Изменить статью')

@section('content')
    <h3 class="pb-3 mb-4 font-italic border-bottom">
        Изменить статью
    </h3>

    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach
    @endif

    <form class="col-md-8" action="{{ route("articles.update", ['article' => $article]) }}" method="post">
        @csrf
        @method('PATCH')
        @include('articles.formFields')
        <button type="submit" class="btn btn-primary">Обновить</button>
    </form>
@endsection
