<div class="mb-3">
    <label for="slug" class="form-label">Символьный код</label>
    <input type="text" class="form-control" id="slug" name="slug" value="{{ old('slug', $article->slug) }}">
</div>
<div class="mb-3">
    <label for="name" class="form-label">Название</label>
    <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $article->name) }}">
</div>
<div class="mb-3">
    <label for="previewText" class="form-label">Краткое описание</label>
    <input type="text" name="previewText" class="form-control" id="previewText"
           value="{{ old('previewText', $article->previewText) }}">
</div>
<div class="mb-3">
    <label for="detailText" class="form-label">Полное описание</label>
    <input type="text" name="detailText" class="form-control" id="detailText" value="{{ old('detailText', $article->detailText) }}">
</div>
<div class="mb-3">
    <label for="tags" class="form-label">Теги</label>
    <input type="text" name="tags" class="form-control" id="tags" value="{{ old('tags', $article->tags->pluck('name')->implode(', ')) }}">
</div>
<div class="mb-3 form-check">
    <label class="form-check-label"  for="published">Опубликовано</label>
    <input type="checkbox" class="form-check-input" name="published" id="published" value="{{ old('published', $article->published) }}">
</div>
