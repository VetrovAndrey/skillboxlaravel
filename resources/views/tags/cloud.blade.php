<div class="p-3 mb-3 bg-light rounded">
    <h4 class="font-italic">Теги</h4>
    <ul class="list-group">
        @foreach($tags as $tag)
            <a href="{{ route('articles.showByTag', ['tag' => $tag->id]) }}">
                <li class="list-group-item">
                    <span class="tag tag-default tag-pill pull-xs-right">{{ $tag->articles->count() }}</span>
                    {{ $tag->name }}
                </li>
            </a>
        @endforeach
    </ul>
</div>
