@extends('layouts.master')

@section('title', 'Обратная связь')

@section('content')
    <h3 class="pb-3 mb-4 font-italic border-bottom">
        Обратная связь
    </h3>

    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach
    @endif

    <form class="col-md-8" action="{{ route("contacts.store") }}" method="post">
        @csrf
        <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}">
        </div>
        <div class="mb-3">
            <label for="message" class="form-label">Сообщение</label>
            <input type="text" class="form-control" id="message" name="message" value="{{ old('message') }}">
        </div>
        <button type="submit" class="btn btn-primary">Создать</button>
    </form>
@endsection
