<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\ArticleController;
use \App\Http\Controllers\ContactController;
use \App\Http\Controllers\AdminController;

Route::get('/', [ArticleController::class, 'index'])->name('main');

Route::prefix('/articles')->name('articles.')->group(function () {
    Route::get('/create', [ArticleController::class, 'create'])->name('create')->middleware('auth');
    Route::get('/{article}', [ArticleController::class, 'show'])->name('detail');
    Route::get('/{article}/edit', [ArticleController::class, 'edit'])->name('edit');
    Route::post('/', [ArticleController::class, 'store'])->name('store');
    Route::delete('/{article}', [ArticleController::class, 'destroy'])->name('destroy');
    Route::patch('/{article}', [ArticleController::class, 'update'])->name('update');
    Route::get('/tag/{tag}', [ArticleController::class, 'showByTag'])->name('showByTag');
});

Route::get('/contacts', [ContactController::class, 'create'])->name('contacts.form');
Route::post('/contacts', [ContactController::class, 'store'])->name('contacts.store');

Route::get('/admin', [AdminController::class, 'index'])->name('admin');

Route::fallback(function () {
    return redirect()->route('main');
});
